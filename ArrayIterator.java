package ArrayIterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * 
 * @author Woramate Jumroonsilp
 *
 * @param <T>
 */
public class ArrayIterator<T> implements Iterator<T>{

	private T[] array;
	private static int cursor = 0;

	public ArrayIterator( T[] array )
	{
		this.array = array;
	}

	/**
	 * 
	 * @return true if 
	 */
	@Override
	public boolean hasNext() 
	{
		if(this.cursor<this.array.length)
		{
			int temp = this.cursor;
			boolean pro = this.next()!=null;
			this.cursor = temp;
			return pro;
		}
		return false;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public T next() 
	{
		if(this.cursor<this.array.length){
			while(this.cursor<this.array.length&&this.array[cursor]==null)
			{
				this.cursor++;
			}
			T pro = this.array[cursor];
			this.cursor++;
			return pro;
		}
		throw new NoSuchElementException();
	}

	/**
	 * 
	 */
	public void remove()
	{
		this.array[cursor]=null;
		if(this.cursor<this.array.length)
			this.cursor++;
	}

	public static void main(String args[])
	{
		String[] array = {"one","two","three",null,"five","six"};

		ArrayIterator<String> iter = new ArrayIterator(array);

		//System.out.println(iter.hasNext());//true
		System.out.println(iter.next()+cursor);//one
		//System.out.println(iter.hasNext());//true
		System.out.println(iter.next()+cursor);//two
		//System.out.println(iter.hasNext());//true
		System.out.println(iter.next()+cursor);//three
		System.out.println(iter.next()+cursor);//five
		iter.remove();
		System.out.println(iter.hasNext());//false
		System.out.println(iter.next()+cursor);//exception
		//System.out.println(iter.hasNext());//true
		//System.out.println(iter.next());//six
		/*System.out.println(iter.hasNext());//exception
		System.out.println(iter.next());*/
	}

}
